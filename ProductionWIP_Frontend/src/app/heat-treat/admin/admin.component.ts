import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Column } from 'src/app/_model/column.model';
import { UserService } from 'src/app/_services/user.service';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { HeatTreatInputService } from 'src/app/_services/heat-treat-input.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ImagePathDto } from 'src/app/_model/DTO/ImagePathDto.model';
import { AppSettings } from 'app-setting';
import { ImageFileDto } from 'src/app/_model/DTO/imageFileDto.model';
import { HeatTreatInput } from 'src/app/_model/HeatTreatInput.model';
import { HeatTreatSearchDto } from 'src/app/_model/DTO/heatTreatSearchDto.model';
import { ImageVisibleDto } from 'src/app/_model/DTO/ImageVisible.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @ViewChild('userForm',{static:false}) userForm:NgForm;
  @ViewChild('searchForm',{static:false}) searchForm:NgForm;


  columnList:Column[]=[];
  userList:any;
  imageList:ImagePathDto;
  updateImageList:ImagePathDto;
  imageVisible:ImageVisibleDto;
  imgRootPath:string;
  modalRef: BsModalRef;
  fileToUpload:ImageFileDto;
  fileToUploadForUpdate:ImageFileDto;
  userUpdateForm:FormGroup;

  operatorNumber:string;
  operatorName:string;
  operatorId:number;

  imageSection: boolean=false;
  updateId: number;
  deleteId: number;
  pageNumber:number=0;
  heatTreatSearch:HeatTreatSearchDto;
  updateModel:any;
  removeImageList:ImagePathDto;

  constructor(
    private userService:UserService,private authService:AuthService,private alertifyService:AlertifyService,
    private heatTreatService:HeatTreatInputService,private modalService: BsModalService) 
   {
    this.imgRootPath=AppSettings.ROOT_IMAGE_PATH;
    this.setOperatorInfo();
    this.heatTreatSearch=new HeatTreatSearchDto();
   }

  ngOnInit() {
    this.getColumn();
    this.getAllHeatTreat();
    this.fileToUpload=new ImageFileDto();
    this.fileToUploadForUpdate=new ImageFileDto();

    this.createUserUpdateForm();
  }
  createUserUpdateForm() {
    this.userUpdateForm = new FormGroup({
      jobCardNumber:new FormControl("",Validators.required),
      firstName:new FormControl("",Validators.required),
      load:new FormControl("",Validators.required),
      stationNumber:new FormControl("",Validators.required),
      temperature:new FormControl("",Validators.required)
    });
  }

  // // convenience getter for easy access to form fields
  // get formControl() { return this.userForm.controls; }

  getColumn() {
    this.userService.getColumns().subscribe(
      response=>{
        this.columnList=response as Array<Column>;
        console.log(this.columnList);
      },
      error=>{
        console.log("Column Getting Problem");
      }
    )
  }

  showImageSection(){
    this.imageSection=true;
  }

  getAllHeatTreat() {
    
    this.heatTreatSearch.pageNumber=this.pageNumber;
    this.heatTreatSearch.date[0]="";
    this.heatTreatSearch.date[1]="";

    this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
      response=>{
        this.userList=response as Array<HeatTreatInput>;
        console.log(this.userList);
        // var item=this.userList[this.userList.length-1];
        // this.pageNumber=this.userList.indexOf(item);
        // console.log(this.pageNumber);
        //this.imagePath=response[0].image.fileContents;
      }
    )
  }

  openImageModal(template:TemplateRef<any>,id:number){
        
    this.imageList=new ImagePathDto();

    var imgList= this.userList.find(x=>x['id']===id);

    this.imageList.image=this.imgRootPath+imgList.image;
    this.imageList.image1=this.imgRootPath+imgList.image1;
    this.imageList.image2=this.imgRootPath+imgList.image2;
    this.imageList.image3=this.imgRootPath+imgList.image3;
    this.imageList.image4=this.imgRootPath+imgList.image4;

    this.modalRef = this.modalService.show(template, { ignoreBackdropClick: false , class:'modal-lg'});
  }

  openUserUpdateModal(template:TemplateRef<any>,id:number){
    this.removeImageList=new ImagePathDto();
    this.updateId=id;
    var model=this.userList.find(x=>x.id==this.updateId);
    this.userUpdateForm.setValue({
      jobCardNumber:model.jobCardNumber,
      firstName:model.firstName,
      load:model.load,
      stationNumber:model.stationNumber,
      temperature:model.temperature
    });

    //show image in update image
    this.updateImageList=new ImagePathDto();
    this.updateImageList.image=AppSettings.ROOT_IMAGE_PATH+model.image;
    this.updateImageList.image1=AppSettings.ROOT_IMAGE_PATH+model.image1;
    this.updateImageList.image2=AppSettings.ROOT_IMAGE_PATH+model.image2;
    this.updateImageList.image3=AppSettings.ROOT_IMAGE_PATH+model.image3;
    this.updateImageList.image4=AppSettings.ROOT_IMAGE_PATH+model.image4;

    //if no image found the hide image section with remove button
    this.imageVisible=new ImageVisibleDto;
    if(model.image==""){
      this.imageVisible.image=false;
    }
    if(model.image1==""){
      this.imageVisible.image1=false;
    }
    if(model.image2==""){
      this.imageVisible.image2=false;
    }
    if(model.image3==""){
      this.imageVisible.image3=false;
    }
    if(model.image4==""){
      this.imageVisible.image4=false;
    }


    this.modalRef = this.modalService.show(template, { ignoreBackdropClick: true , class:'modal-lg'});

  }

  openDeleteModal(template:TemplateRef<any>,id:number){
    this.deleteId=id;
    this.modalRef = this.modalService.show(template, { ignoreBackdropClick: false , class:'modal-md'});

  }

  onUpdate(){

    var formValue=this.userUpdateForm.value;
    if(formValue.firstName==="" && formValue.jobCardNumber==="" && formValue.stationNumber==="" &&
      formValue.stationNumber==="" && formValue.load===""){
      this.alertifyService.error("Please Enter Some Data to add Row");
      return;
    }

    const model=new FormData();
    model.append("id",this.updateId.toString());
    model.append("firstName",formValue.firstName);
    model.append("jobCardNumber",formValue.jobCardNumber);
    model.append("stationNumber",formValue.stationNumber);
    model.append("temperature",formValue.temperature);
    model.append("load",formValue.load);
    model.append("operatorId","");


    //remove existing image

    if(this.removeImageList.image==="remove"){
      model.append("image","remove");
    }
    if(this.removeImageList.image1==="remove"){
      model.append("image1","remove");
    }
    if(this.removeImageList.image2==="remove"){
      model.append("image2","remove");
    }
    if(this.removeImageList.image3==="remove"){
      model.append("image3","remove");
    }
    if(this.removeImageList.image4==="remove"){
      model.append("image4","remove");
    }


    //Bind model with new Uploaded file
    if(this.fileToUploadForUpdate.image!=null){
      model.append("image",this.fileToUploadForUpdate.image,this.fileToUploadForUpdate.image.name);
    }

    if(this.fileToUploadForUpdate.image1!=null){
      model.append("image1",this.fileToUploadForUpdate.image1,this.fileToUploadForUpdate.image1.name);
    }

    if(this.fileToUploadForUpdate.image2!=null){
      model.append("image2",this.fileToUploadForUpdate.image2,this.fileToUploadForUpdate.image2.name);
    }

    if(this.fileToUploadForUpdate.image3!=null){
      model.append("image3",this.fileToUploadForUpdate.image3,this.fileToUploadForUpdate.image3.name);
    }

    if(this.fileToUploadForUpdate.image4!=null){
      model.append("image4",this.fileToUploadForUpdate.image4,this.fileToUploadForUpdate.image4.name);
    }
    


    this.heatTreatService.update(model).subscribe(
      response=>{

          var heatTreat= this.userList.find(x=>x.id==this.updateId);
          var index=this.userList.indexOf(heatTreat);
          this.userList[index]=response;

        this.alertifyService.success("Row Updated Successfully");
        this.fileToUploadForUpdate=new ImageFileDto();
        this.removeImageList=new ImagePathDto();
        this.modalRef.hide();
      },
      error=>{
        this.alertifyService.error("Row Updated Failed !");
      }
    )
  }

  onAddRow(){
    var formValue=this.userForm.value;
    if(formValue.firstName==="" && formValue.jobCardNumber==="" && formValue.stationNumber==="" &&
      formValue.stationNumber==="" && formValue.load===""){
      this.alertifyService.error("Please Enter Some Data to add Row");
      return;
    }

    const model=new FormData();
    model.append("firstName",formValue.firstName);
    model.append("jobCardNumber",formValue.jobCardNumber);
    model.append("stationNumber",formValue.stationNumber);
    model.append("temperature",formValue.temperature);
    model.append("load",formValue.load);
    model.append("operatorId","");

    if(this.fileToUpload.image!=null){
      model.append("image",this.fileToUpload.image,this.fileToUpload.image.name);
    }

    if(this.fileToUpload.image1!=null){
      model.append("image1",this.fileToUpload.image1,this.fileToUpload.image1.name);
    }

    if(this.fileToUpload.image2!=null){
      model.append("image2",this.fileToUpload.image2,this.fileToUpload.image2.name);
    }

    if(this.fileToUpload.image3!=null){
      model.append("image3",this.fileToUpload.image3,this.fileToUpload.image3.name);
    }

    if(this.fileToUpload.image4!=null){
      model.append("image4",this.fileToUpload.image4,this.fileToUpload.image4.name);
    }
    


    this.heatTreatService.create(model,).subscribe(
      response=>{
        this.pageNumber=0;
        this.getAllHeatTreat();
        this.alertifyService.success("Row Added Successfully");
        this.userForm.control.reset();
        this.fileToUpload=new ImageFileDto();
        this.imageSection=false;
        
      },
      error=>{
        this.alertifyService.error("Row Added Failed !");
      }
    )
  }

  onSearch(){

    this.pageNumber=0;
    this.heatTreatSearch.pageNumber=this.pageNumber;
    
    this.searchForm.value.firstName ? this.heatTreatSearch.firstName=this.searchForm.value.firstName : this.heatTreatSearch.firstName="";
    this.searchForm.value.jobCardNumber ? this.heatTreatSearch.jobCardNumber=this.searchForm.value.jobCardNumber : this.heatTreatSearch.jobCardNumber="";
    this.searchForm.value.stationNumber ? this.heatTreatSearch.stationNumber=this.searchForm.value.stationNumber : this.heatTreatSearch.stationNumber="";
    this.searchForm.value.temperature ? this.heatTreatSearch.temperature=this.searchForm.value.temperature : this.heatTreatSearch.temperature="";
    this.searchForm.value.load ? this.heatTreatSearch.load=this.searchForm.value.load : this.heatTreatSearch.load="";
    this.searchForm.value.date ? this.heatTreatSearch.date[0]=this.searchForm.value.date[0] : this.heatTreatSearch.date[0]="";
    this.searchForm.value.date ? this.heatTreatSearch.date[1]=this.searchForm.value.date[1] : this.heatTreatSearch.date[1]="";
    this.searchForm.value.operatorName ? this.heatTreatSearch.operatorName=this.searchForm.value.operatorName : this.heatTreatSearch.operatorName="";


    
    this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
      response=>{
        this.userList=response;
       
      }
    )

  }

  onDelete(){
    this.heatTreatService.delete(this.deleteId).subscribe(
      response=>{
        if(response){
          this.modalRef.hide();
          this.alertifyService.success("Successfully Deleted");
          var user=this.userList.find(x=>x.id===this.deleteId);
          var index=this.userList.indexOf(user);
          this.userList.splice(index,1);
        }
        
      },
      error=>{
        this.modalRef.hide();
      }
    )
  }

  setOperatorInfo() {
    var operator=this.authService.getCurrentOperator();

    this.operatorName=operator['unique_name'];
    this.operatorNumber=operator['certserialnumber'];
    this.operatorId=operator['nameid'];
  }

  onChangeImage(file:FileList,imageNo:number){

    if(imageNo==0){
      this.fileToUpload.image=file.item(0);
    }
    
    if(imageNo==1){
      this.fileToUpload.image1=file.item(0);
    }

    if(imageNo==2){
      this.fileToUpload.image2=file.item(0);
    }
    if(imageNo==3){
      this.fileToUpload.image3=file.item(0);
    }
    if(imageNo==4){
      this.fileToUpload.image4=file.item(0);
    }
    
  }

  onChangeUpdateImage(file:FileList,imageNo:number){

    if(imageNo==0){
      this.fileToUploadForUpdate.image=file.item(0);
    }
    
    if(imageNo==1){
      this.fileToUploadForUpdate.image1=file.item(0);
    }

    if(imageNo==2){
      this.fileToUploadForUpdate.image2=file.item(0);
    }
    if(imageNo==3){
      this.fileToUploadForUpdate.image3=file.item(0);
    }
    if(imageNo==4){
      this.fileToUploadForUpdate.image4=file.item(0);
    }
    
  }

  onScroll() {  
    this.pageNumber=this.pageNumber+1;
    console.log(this.pageNumber);
    this.heatTreatSearch.pageNumber=this.pageNumber;
    this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
      response=>{
        console.log(response);
        var concate=this.userList.concat(response);
        
        this.userList=concate;
        
      }
    )
  }

  removeImage(img:HTMLElement,imgNo){
    img.className="hideDiv";
    
    if(imgNo==="image"){
      this.removeImageList.image="remove";
    }
    if(imgNo==="image1"){
      this.removeImageList.image1="remove";
    }
    if(imgNo==="image2"){
      this.removeImageList.image2="remove";
    }
    if(imgNo==="image3"){
      this.removeImageList.image3="remove";
    }
    if(imgNo==="image4"){
      this.removeImageList.image4="remove";
    }

  }



}
