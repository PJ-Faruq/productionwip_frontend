import { Component, OnInit, TemplateRef, ViewChild, ElementRef, AfterViewInit, ViewChildren } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { UserHideColumns } from 'src/app/_model/UserHideColumns.model';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Column } from 'src/app/_model/column.model';
import { CookieService } from 'ngx-cookie-service';
import { ColumnForViewDto } from 'src/app/_model/DTO/ColumnForViewDto.model';
import { ImageFileDto } from 'src/app/_model/DTO/imageFileDto.model';
import { HeatTreatInput } from 'src/app/_model/HeatTreatInput.model';
import { HeatTreatInputService } from 'src/app/_services/heat-treat-input.service';
import { HeatTreatSearchDto } from 'src/app/_model/DTO/heatTreatSearchDto.model';
import { AppSettings } from 'app-setting';
import { ImagePathDto } from 'src/app/_model/DTO/ImagePathDto.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements AfterViewInit {
  
  @ViewChild('f',{static:false}) columnHideForm:NgForm;
  @ViewChild('autoValueForm',{static:false}) autoValueForm:NgForm;
  @ViewChild('userForm',{static:false}) userForm:NgForm;
  @ViewChild('searchForm',{static:false}) searchForm:NgForm;
  @ViewChild('btnClose',{static:false}) btnClose:ElementRef;
  @ViewChild('template',{static:false}) template:TemplateRef<any>;

  defaultValueForm:FormGroup;

  columns:string[]= [];
  columnList:Column[]=[];
  userList:any;
  imageList:ImagePathDto
  columnForViewList:ColumnForViewDto[]=[];
  userHideColumnList:any;
  modalRef: BsModalRef;
  st:boolean=true;

  operatorNumber:string;
  operatorName:string;
  operatorId:number;
  imageSection: boolean=false;
  fileToUpload:ImageFileDto;

  imgRootPath:string;
  pageNumber:number=0;
  heatTreatSearch:HeatTreatSearchDto;

    constructor(private userService:UserService,private modalService: BsModalService,
      private authService:AuthService,private alertifyService:AlertifyService,
      private cookie:CookieService,private heatTreatService:HeatTreatInputService,
      private router:Router) 
      {
        this.setOperatorInfo();
        this.imgRootPath=AppSettings.ROOT_IMAGE_PATH;
        this.heatTreatSearch=new HeatTreatSearchDto();
      }

      ngOnInit(){
        this.createDefaultValueForm();
      }

      ngAfterViewInit(): void {
        this.openDefaultValueModal();
      }

      ngAfterContentInit(): void {
        

        this.getAllHeatTreat();
        this.fileToUpload=new ImageFileDto();
        setTimeout(() => 
        {
          this.setOperatorInfo();
          this.setCookieValue();
        },
        500);
        

        this.getColumn();
        this.getUserHideColumn();
        
        setTimeout(() => 
        {
          this.setUserFormLabel();
        },
        1000);
        
      }

      createDefaultValueForm(){
        this.defaultValueForm = new FormGroup({
          auto_Load: new FormControl(),
          auto_StationNumber : new FormControl(),
          auto_Temperature:new FormControl()
        });
      }

      checkUndefine(formValue){
        if(formValue.firstName===undefined){
          formValue.firstName="";
        }

        if(formValue.jobCardNumber===undefined){
          formValue.jobCardNumber="";
        }

        if(formValue.stationNumber===undefined){
          formValue.stationNumber="";
        }

        if(formValue.temperature===undefined){
          formValue.temperature="";
        }
        if(formValue.load===undefined){
          formValue.load="";
        }

        return formValue;
      
      }

  
/*............................Get Value Method...............................*/


      getAllHeatTreat() {
    
        this.heatTreatSearch.pageNumber=this.pageNumber;
        this.heatTreatSearch.date[0]="";
        this.heatTreatSearch.date[1]="";
    
        this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
          response=>{
            this.userList=response as Array<HeatTreatInput>;
            
          }
        )
      }

      getUserHideColumn() {
        this.userService.getUserHideColumn(this.operatorId).subscribe(
          response=>{
            this.userHideColumnList=response;
            this.setUserFormLabel();
          },
          error=>{
            this.alertifyService.error(error);
          },
        )
        
      }

      getColumn() {
        this.userService.getColumns().subscribe(
          response=>{
            this.columnList=response as Array<Column>;
            this.columns.push(this.columnList[3].label);
            this.columns.push(this.columnList[4].label);
            this.columns.push(this.columnList[5].label);
          },
          error=>{
            console.log("Column Getting Problem");
          }
        )
      }

/*............................Set Value Method...............................*/

      //Set DefaultValueForm and userForm with the Cookie Value
      setCookieValue(){

        const autoLoad=this.cookie.get("auto_Load"+"-"+this.operatorId);
        var autoStationNumber=this.cookie.get("auto_StationNumber"+"-"+this.operatorId);
        var autoTemperature=this.cookie.get("auto_Temperature"+"-"+this.operatorId);

        this.defaultValueForm.setValue({
          "auto_Load":autoLoad,
          "auto_StationNumber":autoStationNumber,
          "auto_Temperature":autoTemperature
        });

        setTimeout(() => 
          {
            
            if(this.userForm.control.get('jobCardNumber')!=null){
              this.userForm.control.get('jobCardNumber').setValue('');
            }

            if(this.userForm.control.get('firstName')!=null){
              this.userForm.control.get('firstName').setValue(this.operatorName);
            }

            if(this.userForm.control.get('load')!=null){
              this.userForm.control.get('load').setValue(autoLoad);
            }

            if(this.userForm.control.get('stationNumber')!=null){
              this.userForm.control.get('stationNumber').setValue(autoStationNumber);
            }

            if(this.userForm.control.get('temperature')!=null){
              this.userForm.control.get('temperature').setValue(autoTemperature);
            }

            // this.userForm.setValue({
            //   "jobCardNumber":"",
            //   "firstName":this.operatorName,
            //   "load":autoLoad,
            //   "stationNumber":autoStationNumber,
            //   "temperature":autoTemperature
            // });
          },
          500);

        
      }

      //Set User Form Label name and visibility. this method is called in getUserHideColumn();
      setUserFormLabel() {
        this.columnForViewList=[];
        var columnForView=new ColumnForViewDto();
        for(let column of this.columnList){
          var columnForView=new ColumnForViewDto();
          columnForView.label=column.label;
          for(let hideColumnId of this.userHideColumnList){
            if(hideColumnId['columnId']==column.id){
              columnForView.isVisible=false;
            }
          }
          this.columnForViewList.push(columnForView);
        }

      }
    
      setOperatorInfo() {
        var operator=this.authService.getCurrentOperator();
    
        this.operatorName=operator['unique_name'];
        this.operatorNumber=operator['certserialnumber'];
        this.operatorId=operator['nameid'];
      }
    
    
      fillcolumnHideForm() {
        for(let item of this.userHideColumnList){
          this.columnHideForm.control.get('checkbox-'+item['columnId']).setValue(true);
        }
      }

/*............................OnClick & onChange Event Method...............................*/

  //Set Auto Value in Cookie
      onSetValue(){
        const formValue=this.defaultValueForm.value;
        this.cookie.set("auto_Load"+"-"+this.operatorId,formValue.auto_Load,20);
        this.cookie.set("auto_StationNumber"+"-"+this.operatorId,formValue.auto_StationNumber,20);
        this.cookie.set("auto_Temperature"+"-"+this.operatorId,formValue.auto_Temperature,20);

        this.modalRef.hide();
        this.alertifyService.success("Set Default Value Successfully");
        this.setCookieValue();
        console.log(this.cookie.get("auto_Load"+"-"+this.operatorId));
      }

      onSaveChanges(){
        var value=this.columnHideForm.value;
        let columnId: number[] = new Array();

        for(let item of this.columnList){
            if(value['checkbox-'+item.id]){
              columnId.push(item.id)
            }
        }

        var userHideColumn =new UserHideColumns();
        var operator=this.authService.getCurrentOperator();

        userHideColumn.columnId=columnId;
        userHideColumn.operatorId=operator.nameid;
        

        this.userService.createUserHideColumn(userHideColumn).subscribe(
          response=>{
            if(response){
              this.getUserHideColumn();
              this.setCookieValue();
            }
            else{
              this.alertifyService.error("Set Values Failed !");
            }
          },
          error=>{
            this.alertifyService.error(error);
          }
        );

        //This is a trick for closing Bootstrap Modal Using TypeScript
        this.btnClose.nativeElement.click();
        
      }

      onAddRow(){
        var formValue=this.userForm.value;
        const finalFormValue = this.checkUndefine(formValue);
        if(formValue.firstName==="" && formValue.jobCardNumber==="" && formValue.stationNumber==="" &&
          formValue.stationNumber==="" && formValue.load===""){
          this.alertifyService.error("Please Enter Some Data to add Row");
          return;
        }

        const model=new FormData();
        model.append("firstName",formValue.firstName);
        model.append("jobCardNumber",formValue.jobCardNumber);
        model.append("stationNumber",formValue.stationNumber);
        model.append("temperature",formValue.temperature);
        model.append("load",formValue.load);
        model.append("operatorId",this.operatorId.toString());

        if(this.fileToUpload.image!=null){
          model.append("image",this.fileToUpload.image,this.fileToUpload.image.name);
        }

        if(this.fileToUpload.image1!=null){
          model.append("image1",this.fileToUpload.image1,this.fileToUpload.image1.name);
        }

        if(this.fileToUpload.image2!=null){
          model.append("image2",this.fileToUpload.image2,this.fileToUpload.image2.name);
        }

        if(this.fileToUpload.image3!=null){
          model.append("image3",this.fileToUpload.image3,this.fileToUpload.image3.name);
        }

        if(this.fileToUpload.image4!=null){
          model.append("image4",this.fileToUpload.image4,this.fileToUpload.image4.name);
        }
        


        this.heatTreatService.create(model,).subscribe(
          response=>{
            this.pageNumber=0;
            this.getAllHeatTreat();
            this.alertifyService.success("Row Added Successfully");
            this.userForm.control.get('jobCardNumber').setValue('');
            this.fileToUpload=new ImageFileDto();
            this.imageSection=false;
            
          },
          error=>{
            this.alertifyService.error("Row Added Failed !");
          }
        )
      }

      onSearch(){

        this.pageNumber=0;
        this.heatTreatSearch.pageNumber=this.pageNumber;
        
        this.searchForm.value.firstName ? this.heatTreatSearch.firstName=this.searchForm.value.firstName : this.heatTreatSearch.firstName="";
        this.searchForm.value.jobCardNumber ? this.heatTreatSearch.jobCardNumber=this.searchForm.value.jobCardNumber : this.heatTreatSearch.jobCardNumber="";
        this.searchForm.value.stationNumber ? this.heatTreatSearch.stationNumber=this.searchForm.value.stationNumber : this.heatTreatSearch.stationNumber="";
        this.searchForm.value.temperature ? this.heatTreatSearch.temperature=this.searchForm.value.temperature : this.heatTreatSearch.temperature="";
        this.searchForm.value.load ? this.heatTreatSearch.load=this.searchForm.value.load : this.heatTreatSearch.load="";
        this.searchForm.value.date ? this.heatTreatSearch.date[0]=this.searchForm.value.date[0] : this.heatTreatSearch.date[0]="";
        this.searchForm.value.date ? this.heatTreatSearch.date[1]=this.searchForm.value.date[1] : this.heatTreatSearch.date[1]="";
        this.searchForm.value.operatorName ? this.heatTreatSearch.operatorName=this.searchForm.value.operatorName : this.heatTreatSearch.operatorName="";
    
    
        
        this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
          response=>{
            this.userList=response;
           
          }
        )
    
      }

      onChangeImage(file:FileList,imageNo:number){


        if(imageNo==0){
          this.fileToUpload.image=file.item(0);
        }
        
        if(imageNo==1){
          this.fileToUpload.image1=file.item(0);
        }

        if(imageNo==2){
          this.fileToUpload.image2=file.item(0);
        }
        if(imageNo==3){
          this.fileToUpload.image3=file.item(0);
        }
        if(imageNo==4){
          this.fileToUpload.image4=file.item(0);
        }
        console.log(this.fileToUpload);
      }

      loggedOut(){
        localStorage.removeItem('token');
        this.router.navigate(['/OperatorLogin']);
      }

      showImageSection(){
        this.imageSection=true;
      }

      onScroll() {  
        this.pageNumber=this.pageNumber+1;
        console.log(this.pageNumber);
        this.heatTreatSearch.pageNumber=this.pageNumber;
        this.heatTreatService.getAllHeatTreat(this.heatTreatSearch).subscribe(
          response=>{
            console.log(response);
            var concate=this.userList.concat(response);
            
            this.userList=concate;
            
          }
        )
      }

/*............................Open Modal Method...............................*/

      openDefaultValueModal(){
        this.modalRef = this.modalService.show(this.template, { ignoreBackdropClick: false});
      }

      openImageModal(template:TemplateRef<any>,id:number){
        
        this.imageList=new ImagePathDto();

        var imgList= this.userList.find(x=>x['id']===id);

        this.imageList.image=this.imgRootPath+imgList.image;
        this.imageList.image1=this.imgRootPath+imgList.image1;
        this.imageList.image2=this.imgRootPath+imgList.image2;
        this.imageList.image3=this.imgRootPath+imgList.image3;
        this.imageList.image4=this.imgRootPath+imgList.image4;

        this.modalRef = this.modalService.show(template, { ignoreBackdropClick: false , class:'modal-lg'});
      }
}
