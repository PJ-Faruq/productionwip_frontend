import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmbedService } from 'src/app/_services/embed.service';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { UserService } from 'src/app/_services/user.service';
import { Column } from 'src/app/_model/column.model';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { HeatTreatInputService } from 'src/app/_services/heat-treat-input.service';

@Component({
  selector: 'app-embed',
  templateUrl: './embed.component.html',
  styleUrls: ['./embed.component.css']
})
export class EmbedComponent implements OnInit {

  @ViewChild('userForm',{static:false}) userForm:NgForm;
  isValid:boolean=false;
  columnList:Column[]=[];

  constructor(private route:ActivatedRoute,private embedService:EmbedService,
     private userService:UserService,private alertifyService:AlertifyService,
     private heatTreatService:HeatTreatInputService) { }

  ngOnInit() {
    var param=this.route.snapshot.params.Token;
    if(param!=""){
      this.checkToken(param);
    }
    this.getColumn();
    
  }
  checkToken(param:any) {
    this.embedService.getByToken(param).subscribe(
      response=>{
        if(response){
          this.isValid=true;
        }
      }
    )
  }

  getColumn() {
    this.userService.getColumns().subscribe(
      response=>{
        this.columnList=response as Array<Column>;
        console.log(this.columnList);
      },
      error=>{
        this.alertifyService.error(error);
      }
    )
  }

  onAddRow(){
    var formValue=this.userForm.value;
    if(formValue.firstName==="" && formValue.jobCardNumber==="" && formValue.stationNumber==="" &&
      formValue.stationNumber==="" && formValue.load===""){
      this.alertifyService.error("Please Enter Some Data to add Row");
      return;
    }

    const model=new FormData();
    model.append("firstName",formValue.firstName);
    model.append("jobCardNumber",formValue.jobCardNumber);
    model.append("stationNumber",formValue.stationNumber);
    model.append("temperature",formValue.temperature);
    model.append("load",formValue.load);
    model.append("operatorId","");
    


    this.heatTreatService.create(model).subscribe(
      response=>{
        this.alertifyService.success("Row Added Successfully");
        this.userForm.reset();
        
      },
      error=>{
        this.alertifyService.error("Row Added Failed !");
      }
    )
  }
}
