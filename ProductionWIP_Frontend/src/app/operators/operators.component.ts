import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { OperatorService } from '../_services/operator.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Operators } from '../_model/operator.model';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.css']
})
export class OperatorsComponent implements OnInit {

  operatorForm: FormGroup;
  operatorFormUpdate:FormGroup;
  operatorList:any;
  modalRef: BsModalRef;
  updateId:number;
  deleteId:number;

  constructor(private operatorService:OperatorService,private modalService: BsModalService,
    private alertify:AlertifyService) { }

  ngOnInit() {
    this.createOperatorForm();
    this.GetAll();
  }

  onCreate(){
    const formValue=this.operatorForm.value;

      this.operatorService.create(formValue).subscribe(
        response=>{
          if(response){
            this.operatorForm.reset();
            this.alertify.success("Created Successfully");
            this.GetAll();
            //this.operatorList.splice(0,0,formValue);
          }
        },
        error=>{
          this.alertify.error(error);
        }
      )



  }

  onUpdate(){

    var operator=new Operators();
    operator.id=this.updateId;
    operator.name=this.operatorFormUpdate.value.name;
    operator.number=this.operatorFormUpdate.value.number;

    this.operatorService.update(operator).subscribe(
      response=>{
        
        if(response===true){
          this.modalRef.hide();
          this.alertify.success("Updated Successfully");
          var optr= this.operatorList.find(x=>x.id==operator.id);
          var index=this.operatorList.indexOf(optr);
          this.operatorList[index]=operator;
        }


      },
      error=>{
        this.alertify.error(error);
      }
    )

  }

  onDelete(){
    this.operatorService.delete(this.deleteId).subscribe(
      response=>{
        if(response){
          this.modalRef.hide();
          this.alertify.success("Deleted Successfully");
          var oprtr=this.operatorList.find(x=>x.id===this.deleteId);
          var index=this.operatorList.indexOf(oprtr);
          this.operatorList.splice(index,1);
        }
        
      },
      error=>{
        this.modalRef.hide();
        this.alertify.error(error);
      }
    )
  }


  GetAll() {
    this.operatorService.getAll().subscribe(
      response=>{
        this.operatorList=response;
      },

      error=>{
        console.log(error);
      }
    )
  }


  createOperatorForm() {
    this.operatorForm = new FormGroup({
      number: new FormControl("", Validators.required),
      name : new FormControl("",Validators.required)
    });
  }

  createOperatorFormForUpdate(id:any) {
    var operator= this.operatorList.find(x=>x.id===id);

    this.operatorFormUpdate = new FormGroup({
      number: new FormControl(operator.number, Validators.required),
      name : new FormControl(operator.name,Validators.required)
    });
  }


  openUpdateModal(template: TemplateRef<any>,id:any) {
    this.updateId=id;
    this.createOperatorFormForUpdate(id);
    this.modalRef = this.modalService.show(template, {ignoreBackdropClick: true,class: 'modal-md'});
  }

  openDeleteModal(template: TemplateRef<any>,id:any) {
    this.deleteId=id;
    this.modalRef = this.modalService.show(template, {class: 'modal-md'});
  }

}
