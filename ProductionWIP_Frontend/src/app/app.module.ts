import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppComponent } from './app.component';
import { HttpClientModule }    from '@angular/common/http';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { ColumnComponent } from './column/column.component';
import { UserComponent } from './heat-treat/user/user.component';
import { HeatTreatComponent } from './heat-treat/heat-treat.component';
import { OperatorsComponent } from './operators/operators.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAuthComponent } from './user-auth/user-auth.component';
import { OperatorLoginComponent } from './user-auth/operator-login/operator-login.component';
import { AlertifyService } from './_services/alertify.service';
import { AuthGuard } from './_guards/auth.guard';
import { HasRoleDirective } from './_directive/has-role.directive';
import { CookieService } from 'ngx-cookie-service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmbedCodeComponent } from './embed-code/embed-code.component';
import { AdminComponent } from './heat-treat/admin/admin.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AdminLoginComponent } from './user-auth/admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { ErrorInterceptorProvider } from './_common/error.interceptor';
import { JwtModule } from '@auth0/angular-jwt';
import { AppSettings } from 'app-setting';
import { AdminPasswordChangeComponent } from './user-auth/admin-password-change/admin-password-change.component';
import { EmbedComponent } from './heat-treat/embed/embed.component';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    ColumnComponent,
    UserComponent,
    HeatTreatComponent,
    OperatorsComponent,
    UserAuthComponent,
    OperatorLoginComponent,
    HasRoleDirective,
    EmbedCodeComponent,
    AdminComponent,
    AdminLoginComponent,
    AdminHomeComponent,
    AdminPasswordChangeComponent,
    EmbedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    FormsModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    InfiniteScrollModule,
    JwtModule.forRoot({
      config:{
        tokenGetter:tokenGetter,
        whitelistedDomains:[AppSettings.DOMAIN_NAME],
        blacklistedRoutes:[]
        //blacklistedRoutes:[AppSettings.DOMAIN_NAME+'/api/auth']
      }
    })
  ],
  providers: [CookieService,
    ErrorInterceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
