import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { AuthService } from 'src/app/_services/auth.service';
import { ChangePasswordDto } from 'src/app/_model/DTO/changePasswordDto.model';

@Component({
  selector: 'app-admin-password-change',
  templateUrl: './admin-password-change.component.html',
  styleUrls: ['./admin-password-change.component.css']
})
export class AdminPasswordChangeComponent implements OnInit {

  changePasswordForm:FormGroup
  constructor(private alertify:AlertifyService,private authService:AuthService) { }

  ngOnInit() {
    this.createChangePasswordForm();
  }
  createChangePasswordForm() {
    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl("",Validators.required),
      newPassword: new FormControl("",[Validators.required,Validators.minLength(4)]),
      confirmPassword: new FormControl("",Validators.required),
    });
  }

  onChangePassword(){
    const formValue= this.changePasswordForm.value;

    if(formValue.newPassword != formValue.confirmPassword){
        this.alertify.error("New Password and Confirm Password are not Matched !");
        return;
    }

    if(formValue.newPassword===formValue.currentPassword){
      this.alertify.error("Your Current Password and New Password are same");
      return;
    }

    var changePassModel=new ChangePasswordDto();
    changePassModel.currentPassword=formValue.currentPassword;
    changePassModel.newPassword=formValue.newPassword;

    this.authService.changeAdminPassword(changePassModel).subscribe(
      response=>{
        if(response){
          this.alertify.success("Password Changed Successfully");
          this.changePasswordForm.reset();
        }
        else{
          this.alertify.error("Wrong Current Password !");
        }
      },
      error=>{
        this.alertify.error(error);
      }
    )


  }

   // convenience getter for easy access to form fields
   get formControl() { return this.changePasswordForm.controls; }





}
