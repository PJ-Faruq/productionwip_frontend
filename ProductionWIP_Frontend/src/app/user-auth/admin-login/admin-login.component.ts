import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  adminLoginForm: FormGroup;
  constructor(private authService:AuthService,private alertify:AlertifyService,private router:Router,) {

        if(this.authService.roleMatch(['Admin'])){
        this.router.navigate(['/AdminHome']);
        return;
    }
   }

  ngOnInit() {
    this.createOperatorForm();
  }

  createOperatorForm() {
    this.adminLoginForm = new FormGroup({
      username: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });
  }

  onLogin(){
      const formValue=this.adminLoginForm.value;
      this.authService.adminLogin(formValue).subscribe(
        next=>{
          console.log(next);
          if(next){
            this.router.navigate(['/AdminHome']);
          }
          else{
            this.alertify.error("Wrong Username or Password");
          }
          
        },
        error=>{
          this.alertify.error(error);
        }
      );

  }

}
