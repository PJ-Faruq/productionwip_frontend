import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-operator-login',
  templateUrl: './operator-login.component.html',
  styleUrls: ['./operator-login.component.css']
})
export class OperatorLoginComponent implements OnInit {

  operatorLoginForm: FormGroup;
  constructor(private authService:AuthService,private router:Router,private alertify:AlertifyService) {

    if(this.authService.roleMatch(['Operator'])){
      this.router.navigate(['/HeatTreat/User']);
    }
   }

  ngOnInit() {
    this.createOperatorForm();
  }

  createOperatorForm() {
    this.operatorLoginForm = new FormGroup({
      number: new FormControl("", Validators.required)
    });
  }

  onLogin(){
      const formValue=this.operatorLoginForm.value;
      this.authService.operatorLogin(formValue).subscribe(
        next=>{
          if(next){
            this.router.navigate(['/HeatTreat/User']);
            
          }
          else{
            this.alertify.error("Wrong Operator Number");
          }
          
         
        },
        error=>{
          this.alertify.error(error);
        }
      );

  }
}
