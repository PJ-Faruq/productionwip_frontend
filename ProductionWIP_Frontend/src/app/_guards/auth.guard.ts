import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService:AuthService,
    private router:Router, 
    private alertify:AlertifyService){}

  canActivate(route:ActivatedRouteSnapshot): boolean {

    //const roles=route.firstChild.data['roles'] as Array<string>;
    const roles=route.data.roles;
    console.log(roles);
    if(roles){
      const match=this.authService.roleMatch(roles);
      if(match){
        return true;
      }
      else{
        this.alertify.error("You Have no Permission");
        this.router.navigate(['/Home']);
        return false;
      }
    }



    if(this.authService.loggedIn()){
      return true;
    }

    this.alertify.error("You Have no Permission");
    this.router.navigate(['/Home']);
    return false;
  }
  
}
