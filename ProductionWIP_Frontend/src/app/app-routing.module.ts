import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router'
import { HomeComponent } from './home/home.component';
import { ColumnComponent } from './column/column.component';
import { HeatTreatComponent } from './heat-treat/heat-treat.component';
import { UserComponent } from './heat-treat/user/user.component';
import { OperatorsComponent } from './operators/operators.component';
import { OperatorLoginComponent } from './user-auth/operator-login/operator-login.component';
import { AuthGuard } from './_guards/auth.guard';
import { EmbedCodeComponent } from './embed-code/embed-code.component';
import { AdminComponent } from './heat-treat/admin/admin.component';
import { AdminLoginComponent } from './user-auth/admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminPasswordChangeComponent } from './user-auth/admin-password-change/admin-password-change.component';
import { EmbedComponent } from './heat-treat/embed/embed.component';


const appRoutes:Routes=[
    { path:'Home',component:HomeComponent },
    { path:'',component:HomeComponent },
    { path:'',
      runGuardsAndResolvers:"always",
      canActivate:[AuthGuard],data:{roles:['Admin']},
      children:[
         //{ path:"Exam" ,component:ExamComponent, data:{roles:['Admin','SuperUser']} },
         { path:'Columns', component:ColumnComponent },
         { path:'Operators', component:OperatorsComponent},
         { path:'EmbedCodes', component:EmbedCodeComponent},
      ] },
    
    { path:"HeatTreat", component:HeatTreatComponent,
     children:[
        { path:"User" , component: UserComponent, canActivate:[AuthGuard], data:{roles:['Operator']}},
        { path:"Admin" , component: AdminComponent,canActivate:[AuthGuard],data:{roles:['Admin']} },
        { path:"Embed/:Token" , component: EmbedComponent }
     ] },
     
     { path:'OperatorLogin', component:OperatorLoginComponent },
     { path:'AdminLogin', component:AdminLoginComponent },
     { path:'AdminHome', component:AdminHomeComponent,canActivate:[AuthGuard],data:{roles:['Admin']}  },
     { path:'ChangePassword', component:AdminPasswordChangeComponent,canActivate:[AuthGuard],data:{roles:['Admin']}  }
     

]

@NgModule({
imports:[RouterModule.forRoot(appRoutes)],
exports: [RouterModule]
})

export class AppRoutingModule{

}