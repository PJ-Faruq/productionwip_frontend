import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EmbedService } from '../_services/embed.service';
import { EmbedCode } from '../_model/EmbedCode.model';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-embed-code',
  templateUrl: './embed-code.component.html',
  styleUrls: ['./embed-code.component.css']
})
export class EmbedCodeComponent implements OnInit {

  embedForm: FormGroup;
  embedFormUpdate:FormGroup;
  embedList:EmbedCode[];
  modalRef: BsModalRef;
  updateId:number;
  deleteId:number;

  constructor(private embedService:EmbedService,private modalService: BsModalService,
    private alertify:AlertifyService) { }

  ngOnInit() {
    this.createOperatorForm();
    this.GetAll();
  }

  onCreate(){
    const formValue=this.embedForm.value;

      this.embedService.create(formValue).subscribe(
        response=>{
          if(response){
            this.alertify.success("Embed Code Successfully Added");
            this.embedForm.reset();
            this.GetAll();
            //this.embedList.splice(0,0,formValue);
          }
        },
        error=>{
          this.alertify.error(error);
        }
      )



  }

  onUpdate(){

    var embed=new EmbedCode();
    embed.id=this.updateId;
    embed.identifier=this.embedFormUpdate.value.identifier;
    embed.token=this.embedFormUpdate.value.token;

    this.embedService.update(embed).subscribe(
      response=>{

        if(response===true){
          this.modalRef.hide();
          this.alertify.success("Updated Successfully");
          var embd= this.embedList.find(x=>x.id==embed.id);
          var index=this.embedList.indexOf(embd);
          this.embedList[index]=embed;
        }


      },
      error=>{
        this.alertify.error(error);
      }
    )

  }

  onDelete(){
    this.embedService.delete(this.deleteId).subscribe(
      response=>{
        if(response){
          this.modalRef.hide();
          this.alertify.success("Deleted Successfully");
          var embd=this.embedList.find(x=>x.id===this.deleteId);
          var index=this.embedList.indexOf(embd);
          this.embedList.splice(index,1);
        }
        
      },
      error=>{
        this.modalRef.hide();
        this.alertify.error(error);
      }
    )
  }


  GetAll() {
    this.embedService.getAll().subscribe(
      (response:EmbedCode[])=>{
        this.embedList=response;
        console.log(this.embedList);
      },

      error=>{
        console.log(error);
      }
    )
  }


  createOperatorForm() {
    this.embedForm = new FormGroup({
      identifier: new FormControl("", Validators.required),
      token : new FormControl("",Validators.required)
    });
  }

  createEmbedFormForUpdate(id:any) {
    var embed= this.embedList.find(x=>x.id===id);

    this.embedFormUpdate = new FormGroup({
      identifier: new FormControl(embed.identifier, Validators.required),
      token : new FormControl(embed.token,Validators.required)
    });
  }


  openUpdateModal(template: TemplateRef<any>,id:any) {
    this.updateId=id;
    this.createEmbedFormForUpdate(id);
    this.modalRef = this.modalService.show(template, {ignoreBackdropClick: true,class: 'modal-md'});
  }

  openDeleteModal(template: TemplateRef<any>,id:any) {
    this.deleteId=id;
    this.modalRef = this.modalService.show(template, {class: 'modal-md'});
  }

}
