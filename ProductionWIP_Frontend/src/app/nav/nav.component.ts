import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  loggedIn(){
   return this.authService.loggedIn();
  }
  loggedOut(){
    var isAdmin=this.matchRole(['Admin']);
    localStorage.removeItem('token');
    if(isAdmin){
      this.router.navigate(['/Home']);
    }
    else{
      this.router.navigate(['/OperatorLogin']);
    }
    
  }

  matchRole(roles:any){
    return this.authService.roleMatch(roles);
  }

}
