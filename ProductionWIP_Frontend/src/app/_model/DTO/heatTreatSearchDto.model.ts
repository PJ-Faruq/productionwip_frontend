export class HeatTreatSearchDto{
    jobCardNumber:string= "";
    firstName:string= "";
    stationNumber:string= "";
    temperature:string= "";
    date:string[]= [];
    load:string= "";
    operatorName:string= "";
    pageNumber:number;
}