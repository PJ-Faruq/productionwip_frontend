import { Component, OnInit, TemplateRef } from '@angular/core';
import { ColumnService } from '../_services/column.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Column } from '../_model/column.model';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.css']
})
export class ColumnComponent implements OnInit {

  columnList:any;
  modalRef: BsModalRef;
  columnFormUpdate:FormGroup;
  updateItem:any;

  constructor(private columnService:ColumnService,private modalService: BsModalService,
    private alertify:AlertifyService) { }

  ngOnInit() {

    this.getColumns();
  }


  getColumns() {
     this.columnService.getColumns().subscribe(
       response=>{
         console.log(response);
        this.columnList=response;
       },
       error=>{
         this.alertify.error(error);
       }
     )
  }

  onUpdate(){
    var column =new Column();
    column.id=this.updateItem.id;
    column.label=this.columnFormUpdate.value.label;
    column.order=this.columnFormUpdate.value.order;

    this.columnService.update(column).subscribe(
      response=>{
        if(response){
          this.modalRef.hide();
          var colum= this.columnList.find(x=>x.id==column.id);
          var index=this.columnList.indexOf(colum);
          this.columnList[index]=column;
        }
      }
    )
  }

  openUpdateModal(template: TemplateRef<any>,item:any) {

    this.updateItem=item;
    this.createColumnFormForUpdate(item);
    this.modalRef = this.modalService.show(template, { ignoreBackdropClick: true,class: 'modal-md'});
  }

  createColumnFormForUpdate(item: any) {
    this.columnFormUpdate = new FormGroup({
      label: new FormControl(item.label, [Validators.required]),
      order : new FormControl(item.order,Validators.required)
    });
  }


}
