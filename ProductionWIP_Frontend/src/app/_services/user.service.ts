import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ColumnService } from './column.service';
import { AppSettings } from 'app-setting';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient,private columnService:ColumnService) { }

  getColumns(){
    return this.columnService.getColumns();
  }

  getUserHideColumn(operatorId:number){
    return this.httpClient.get(AppSettings.BASE_URL+"HeatTreat/GetColumnsToHideUser"+'/'+operatorId);
  }

  createUserHideColumn(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"HeatTreat/CreateColumnsToHideUser",model);
  }
}
