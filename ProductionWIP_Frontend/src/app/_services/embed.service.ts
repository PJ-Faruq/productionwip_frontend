import { Injectable } from '@angular/core';
import { AppSettings } from 'app-setting';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmbedService {

  constructor(private httpClient:HttpClient) { }

  create(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+'EmbedCode',model);
  }

  getAll(){
    return this.httpClient.get(AppSettings.BASE_URL+'EmbedCode');
  }

  update(model:any){
    return this.httpClient.put(AppSettings.BASE_URL+'EmbedCode',model);
  }

  delete(id:number){
    return this.httpClient.delete(AppSettings.BASE_URL+'EmbedCode'+'/'+id);
  }

  getByToken(token:string){
    return this.httpClient.get(AppSettings.BASE_URL+'EmbedCode'+'/'+token);
  }
}
