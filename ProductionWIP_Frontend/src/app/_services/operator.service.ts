import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from 'app-setting';

@Injectable({
  providedIn: 'root'
})
export class OperatorService {

  constructor(private httpClient:HttpClient) { }

  create(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+'operator',model);
  }

  getAll(){
    return this.httpClient.get(AppSettings.BASE_URL+'operator');
  }

  update(model:any){
    return this.httpClient.put(AppSettings.BASE_URL+'operator',model);
  }

  delete(id:number){
    return this.httpClient.delete(AppSettings.BASE_URL+'operator'+'/'+id);
  }
}
