import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from 'app-setting';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AlertifyService } from './alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper=new JwtHelperService();
  decodedToken:any;

  constructor(private httpClient:HttpClient,private alertify:AlertifyService) { }

  operatorLogin(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"Auth/OperatorLogin",model).pipe(
      map((response:any)=>{
        const user=response;
        if(user){
          localStorage.setItem('token',user.token);
          this.decodedToken=this.jwtHelper.decodeToken(user.token);
          return true;
        }
        if(user===false){
          return false;
        }
      })
    );
  }

  adminLogin(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"Auth/AdminLogin",model).pipe(
      map((response:any)=>{
        const user=response;
        if(user){
          localStorage.setItem('token',user.token);
          this.decodedToken=this.jwtHelper.decodeToken(user.token);
          return true;
        }
        if(user===false){
          return false;
        }

      })
    );
  }

  loggedIn(){
    const token=localStorage.getItem("token");
    return !this.jwtHelper.isTokenExpired(token);
  }

    roleMatch(allowedRoles):boolean{
    let isMatch=false;
    const userRoles:string[]=this.getUserRoles();
    if(userRoles != null){
      allowedRoles.forEach(element => {
        if(userRoles.includes(element)){
          isMatch=true;
          return;
        }
      });

    }
    return isMatch;

  }

  getUserRoles(){
    const token =localStorage.getItem("token");
    if(token!=null){
      let decodedToken=this.jwtHelper.decodeToken(token);
      return decodedToken.role as Array<string>;
    }
    return null;    
  }

  getCurrentOperator(){
    var token=localStorage.getItem('token');
    return this.jwtHelper.decodeToken(token);
  }

  changeAdminPassword(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"Auth/ChangeAdminPassword",model);
  }
}
