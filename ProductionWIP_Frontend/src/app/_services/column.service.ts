import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from 'app-setting';

const httpOption={
  headers:new HttpHeaders({
    'Authorization': 'Bearer '+localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class ColumnService {

  constructor(private httpClient:HttpClient) {

   }



   getColumns(){
     return this.httpClient.get(AppSettings.BASE_URL+"column");
   }

   update(model:any){
     return this.httpClient.put(AppSettings.BASE_URL+"column",model);
   }
}
