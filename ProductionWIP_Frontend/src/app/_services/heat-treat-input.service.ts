import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from 'app-setting';


@Injectable({
  providedIn: 'root'
})
export class HeatTreatInputService {



  constructor(private httpClient:HttpClient) { }

  create(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"HeatTreat",model);
  }

  getAllHeatTreat(model:any){
    return this.httpClient.post(AppSettings.BASE_URL+"HeatTreat/GetAll",model);
  }

  update(model:any){
    return this.httpClient.put(AppSettings.BASE_URL+"HeatTreat",model);
  }

  delete(id: number) {
    return this.httpClient.delete(AppSettings.BASE_URL+'HeatTreat'+'/'+id);
  }

}
