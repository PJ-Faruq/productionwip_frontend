import { Directive, Input, ViewContainerRef, TemplateRef, OnInit } from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { AuthService } from '../_services/auth.service';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit {
  

  @Input() appHasRole:string[];
  isVisible=false;

  constructor(
    private viewContainerRef:ViewContainerRef,
    private templateRef:TemplateRef<any>,
    private authService:AuthService) { }

    ngOnInit(){
      
      const userRoles=this.authService.getUserRoles();


      //If there is no Role is Found
      if(!userRoles){
        this.viewContainerRef.clear();
      }

      //If there has any Role 
      if(this.authService.roleMatch(this.appHasRole)){
        if(!this.isVisible){
          this.isVisible=true;
          this.viewContainerRef.createEmbeddedView(this.templateRef);
        }

        else
        {
          this.isVisible=false;
          this.viewContainerRef.clear();
        }
      }
    }

}
