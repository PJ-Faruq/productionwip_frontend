import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS } from "@angular/common/http"
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable()
export class ErrorInterceptor implements HttpInterceptor{
    intercept(request:HttpRequest<any>,next: HttpHandler):Observable<HttpEvent<any>>{
        return next.handle(request).pipe(
            catchError(error => {
                if(error instanceof HttpErrorResponse){
                    if(error.statusText==="Unknown Error"){
                        return throwError("Server is Offline");
                    }
                    return throwError(error.statusText); 
                }

                
            })
        )
    }

}

export const ErrorInterceptorProvider={

    provide:HTTP_INTERCEPTORS,
    useClass:ErrorInterceptor,
    multi:true
};